# from django.contrib.sites import requests
import requests
from requests import ConnectionError


def get_connection():
    json_input={
        "inputText" : "hello"
    }
    try:
        res = requests.post("http://localhost:8080/reviewSummariser/summarise/50",json=json_input)
    except ConnectionError:
        return 404

    return res.status_code

def test_connection():
    res=get_connection()
    assert 200 == res
