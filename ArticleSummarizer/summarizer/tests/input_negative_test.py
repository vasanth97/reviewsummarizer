import requests
from requests import ConnectionError


def send_input():
    json_input={
        "randome_key":"hello"
    }
    try:
        res=requests.post("http://localhost:8080/reviewSummariser/summarise/50",json=json_input)
    except ConnectionError:
        return 404

    return res.status_code

def test_incorrect_input():
    assert 500==send_input()


