import requests
import json
from requests import ConnectionError


def get_summary(length,json_input):

    try:
        res=requests.post("http://localhost:8080/reviewSummariser/summarise/"+str(length),json=json_input)
    except ConnectionError:
        return 404
    print res.text, res.status_code
    return json.loads(res.text)["summary"]

def test_correct_input():
    json_input = {
        "inputText": "Good morning. Have a nice day."
    }
    summary=get_summary(50,json_input)

    summary_in_arr=summary.split(".")
    for each in summary_in_arr:
        if each == ' ':
            del summary_in_arr[summary_in_arr.index(each)]
    print summary_in_arr
    assert 1 == len(summary_in_arr)


