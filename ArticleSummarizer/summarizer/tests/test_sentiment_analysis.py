
from summarizer.utils import getSentimentScore

def test_sentiment_analysis():
    sentiment_score= getSentimentScore("This coffee is amazing. Totally worth for the price. Made my day.")
    if "neg" not in sentiment_score or "neu" not in sentiment_score or "pos" not in sentiment_score or "compound" not in sentiment_score:
        assert False
    for each in sentiment_score:
        if sentiment_score[each] <0 or sentiment_score[each]>1:
            assert False

    assert True

