# -*- coding: iso-8859-15 -*-
from __future__ import unicode_literals

import json

import requests
import time
from django.shortcuts import render
from django.contrib.sites import requests
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, context
from requests import ConnectionError

from OCR import Ocr
from forms import InputForm, Home, Images
import requests

# Create your views here.
from utils import LanguageUtilities, SentimentalAnalysis


def index(request):
    # template=loader.get_template("templates/home.html")
    context = {}
    if request.method == 'POST':
        form = InputForm(request.POST)

        context['inputText'] = form['input'].value()
        percentage=request.POST.get("sliderInput")
        percentage=str(percentage)
        percentage=percentage.replace('%','')
        template = loader.get_template('templates/summarise.html')
        inputText = context["inputText"]
        englishText = inputText
        language_utitlities=LanguageUtilities()
        lan = language_utitlities.getLanguage(inputText.split("."))
        print "language",lan
        if lan != "en":
            englishText = language_utitlities.translater(inputText, lan)
            print "english",englishText


        # context['language']='ru'
        url="http://localhost:8080/reviewSummariser/summarise/"+percentage

        context['inputText'] = englishText
        try:
            r=requests.post(url, json=context)
            print r
        except ConnectionError:
            template = loader.get_template('templates/error_summarise.html')
            return HttpResponse(template.render({}, request))
        sentiment_analysis=SentimentalAnalysis()
        sentimentScoreJson=sentiment_analysis.getSentimentScore(englishText)
        sentimentScore=""
        for each in sentimentScoreJson:
            sentimentScore += each + ":" + " " + str(sentimentScoreJson[each]) + "\n"
        print(r.status_code, r.text)
        output = json.loads(r.text)
        # print output
        context["output"] = output["summary"]
        context["keywords"]=output["keywords"]
        context["sentimentScore"]=sentimentScore
        return HttpResponse(template.render(context, request))

    context = {}
    context['input'] = ""
    context['output'] = ""
    template = loader.get_template('templates/summarise.html')
    return HttpResponse(template.render(context, request))


def home(request):
    template = loader.get_template('templates/home.html')
    form = Home(request.POST)
    var = form['option'].value()
    context = {}
    if var == 'text':
        return HttpResponseRedirect('/summarize/text')
    if var == 'image':
        return HttpResponseRedirect('/summarize/image')

    return HttpResponse(template.render(context, request))


def image(request):
    form = Images(request.POST, request.FILES)
    string = ""
    template = loader.get_template('templates/summarise_img.html')
    print request.POST.get("lang")
    language= request.POST.get("lang")
    #todo this is language in image
    context = {}
    if request.FILES:
        with open('/Users/akshayshenoy/Desktop/TEMP/img.png', 'wb+') as destination:
            for chunk in request.FILES['img1'].chunks():
                destination.write(chunk)
        string = Ocr(language).output
        # template = loader.get_template('templates/summarise_img.html')
        if language != "en":
            language_utitlities = LanguageUtilities()
            string = language_utitlities.translater(string, language)
        context = {}
        context['inputText'] = string
        percentage=request.POST.get("sliderInput")
        percentage = str(percentage)
        percentage = percentage.replace('%', '')
        url="http://localhost:8080/reviewSummariser/summarise/"+percentage
        try:
            r=requests.post(url,json=context)
        except ConnectionError:
            template = loader.get_template('templates/error_summarise_image.html')
        output = json.loads(r.text)
        sentiment_analysis=SentimentalAnalysis()
        sentimentScoreJson = sentiment_analysis.getSentimentScore(string)
        sentimentScore = ""
        for each in sentimentScoreJson:
            sentimentScore += each + ":" + " " + str(sentimentScoreJson[each]) + "\n"

        context["output"] = output['summary']
        context["keywords"] = output["keywords"]
        context["sentimentScore"] = sentimentScore
    return HttpResponse(template.render(context, request))
