from django import forms


# our new form
class InputForm(forms.Form):
    input = forms.CharField(widget=forms.TextInput())


class Home(forms.Form):
    option = forms.CharField(widget=forms.TextInput())


class Images(forms.Form):
    img1 = forms.FileField()
