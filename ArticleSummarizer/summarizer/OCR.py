import pytesseract
import cv2
import numpy as np
from PIL import Image
from forms import Images
# -*- coding: iso-8859-15 -*-

class Ocr:

    def __init__(self,language):
        self.output = self.imageToText(language)

    def imageToText(self,language):


        # Read image with opencv
        img = cv2.imread('/Users/akshayshenoy/Desktop/TEMP/img.png')

        # Convert to gray
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Apply dilation and erosion to remove some noise
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)
        img = cv2.erode(img, kernel, iterations=1)

        # Write image after removed noise
        cv2.imwrite("/Users/akshayshenoy/Desktop/TEMPremoved_noise.png", img)

        # Write the image after apply opencv to do some ...
        cv2.imwrite("/Users/akshayshenoy/Desktop/TEMP,thres.png", img)

        # Recognize text with tesseract for python
        result = pytesseract.image_to_string(Image.open("/Users/akshayshenoy/Desktop/TEMP,thres.png"), lang=language)

        return result

