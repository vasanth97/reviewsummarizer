import subprocess
from langdetect import detect
from langdetect import detect_langs
import langid
from nltk.sentiment.vader import SentimentIntensityAnalyzer

class LanguageUtilities:


    def getLanguage(self,string):
        for word in string:
            result = langid.classify(word)
            return result[0]


    def translater(self,result,lan):
        #nltk.download('vader_lexicon')
        print "language : " + lan
        final_result = ""
        sent_text = result.split(".")
        for sentence in sent_text:
            print sentence
            output = subprocess.check_output(["translate-cli","-t","en",sentence,"-o"])
            final_result += output
        return final_result

class SentimentalAnalysis:
    def getSentimentScore(self,inputString):
        result=""
        sid = SentimentIntensityAnalyzer()
        for sentence in [inputString]:
            ss = sid.polarity_scores(sentence)
            print ss

            return ss