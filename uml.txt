Review Summarizer

textSummarizer

interfaces:
	IKeywordAlgorithm
		List<String> getKeywords(List<List<String>> sentences)
	ISummarizationAlgorithm
		List<Integer> getSelection(List<List<String>> sentences, int percentage)

classes
	HitsAlgorithm implements IKeywordAlgorithm
		List<String> getKeywords(List<List<String>> sentences)
		List<String> makeWordList(List<List<String>> sentences) 
		Map<Integer, HITSNode> makeGraph(List<List<String>> sentences,
			List<String> wordList)
		
		List<Integer> runHITS(Map<Integer, HITSNode> graph, int k)
 		List<String> makeKeywordList(List<Integer> ordered, List<String> words)
	
	HitsNode
		List<Integer> incoming;
		List<Integer> outgoing;
		void addIncoming(int value)
		void addOutgoing(int value)
		List<Integer> getIncoming() 
		
		public List<Integer> getOutgoing() 

	IndexValuePair
		int compareTo(IndexValuePair c)
	
	NaiveBayesAlgorithm implements ISummarizationAlgorithm
		public NaiveBayesAlgorithm() 
		private void initModel() 

		public List<Integer> getSelection(List<List<String>> sentences, int percentage) 
	
		private void buildModel(List<List<String>> sentences)
		private List<Integer> makeSummarySelection(List<IndexValuePair> sentenceScores,
				int percent) 
		
		private List<IndexValuePair> makeSentenceScores(List<Double> docCentroidValues,
				List<Double> positionalValues, List<Integer> overlaps)
		 
		private List<List<Integer>> makeSentenceVectors(
				List<List<String>> sentences, List<String> sentenceTerms) 
		private List<Integer> makeFirstSentenceOverlaps(List<List<Integer>> sentenceVectors) 
		private List<Double> makePositionalValues(int size, double maxCentroidValue)

	
		private List<Double> makeDocumentCentroids(List<List<String>> sentences,
				List<Double> centroidValues,
				List<String> centroidDoc) 
		private void makeDocFrequencies(List<List<String>> sentences)
		private void makeTerms() 
		private void makeAverageTermFrequencies(List<List<String>> sentences)
		private List<Double> makeCentroidValues(int numSentences) 
		private List<String> makeCentroidDocument(List<Double> centroidValues) 
	DocumentSummarizer 
		public DocumentSummarizer(SentenceSegmenter segmenter
				SentencePreprocessor preprocessor) 
		public String summarize(String text, int percentage) 
		private String buildSummaryString(List<String> sentences

	KeywordExtractor 
		SentenceSegmenter segmenter;
		SentencePreprocessor preprocessor;
		IKeywordAlgorithm hits;

		public KeywordExtractor(SentenceSegmenter segmenter
				SentencePreprocessor preprocessor) 
		public String extract(String text) 
		private String makeKeywordString(List<String> keywords, int k) 

	SentencePreprocessor 
	
		List<String> stopwords;
	
	
		public List<List<String>> process(List<List<String>> document) 
		public List<List<String>> removeStopwords(List<List<String>> document) 
		public List<List<String>> removePunctuation(List<List<String>> document)
		public List<List<String>> makeLowercase(List<List<String>> document) 
		public List<String> readStopwords() 

	SentenceSegmenter 
	
		public List<List<String>> segment(String text)
		public List<String> getOriginalSentences (String text) 

Tesseract
	ImageToText
		String getText(String filename)
